const processArray = require('../src/index')

describe('Array treatment', () => {
  it('Should remove all zeros', () => {
    const testArray = [[7, 0, 2, 1, 0, 1], [3, 0, 0, 2]]

    const procesedArray = processArray(testArray)

    expect(procesedArray).toEqual([[7, 2, 1, 1], [3, 2]])
  })
})