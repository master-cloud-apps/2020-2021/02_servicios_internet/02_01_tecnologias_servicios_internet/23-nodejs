const request = require('supertest')
const app = require('./../src/app')

describe('Server', () => {
  describe('Endpoints', () => {
    describe('Home GET', () => {
      it('Render the home', async() => {
        const response = await request(app)
          .get('/')
        expect(response.statusCode).toEqual(200)
        expect(response.text).toEqual(expect.stringContaining('Hello Luke'))
      })
    })
  })
})
